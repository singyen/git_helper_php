# 目录结构

```
.
├── config
│   ├── config.php          #配置文件,一般不需要修改
│   └── git.config.php      #GIT配置文件,查看GOGS WEB钩子消息设置
├── data                    #数据存储目录
│   ├── index.html
├── index.php                  
├── lib
│   ├── db.php              #文件存储操作
│   ├── Git.php             #执行GIT命令
│   ├── Http.php            #网络请求
│   ├── Log.php             #日志操作
│   ├── main.php            #主要控制逻辑
│   └── sign.php            #签名生成
├── LICENSE
├── logs                    #运行和错误日志目录
│   ├── index.html
└── README.md
```
