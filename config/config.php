<?php
set_time_limit(3600);
error_reporting(E_ALL);
define(ROOT_PATH,str_replace("config","",dirname(__FILE__)));
ini_set('error_log', ROOT_PATH.'/logs/'.'error_'.date('Ymd').'.log');
include (ROOT_PATH.'/lib/db.php');
include (ROOT_PATH.'/lib/Log.php');
include (ROOT_PATH.'/lib/sign.php');
include (ROOT_PATH.'/lib/Git.php');
include (ROOT_PATH.'/lib/Http.php');
include (ROOT_PATH.'/lib/main.php');