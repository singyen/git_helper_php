<?php
/**
 * 网络请求
 * */
class Http{

    public static function Post($url,$data,$header=array('Content-Type:application/json;charset=utf-8;')){
        $ch= curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER,$header); 
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);//是否直接输出
        curl_setopt($ch, CURLOPT_POST,true);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
        $result=curl_exec($ch);
        curl_close($ch);
        return $result;
    }
}