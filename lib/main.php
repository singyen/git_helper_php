<?php
/**
 * 主要控制逻辑
 * */
class main{
    /**
     * 运行入口
     * */
    public function run(){
         $action=isset($_GET['action'])?$_GET['action']:"";
         $git_config=db::get_git_config();
         $webhooks = file_get_contents('php://input');
         $sign=isset($_SERVER['HTTP_X_GOGS_SIGNATURE'])?$_SERVER['HTTP_X_GOGS_SIGNATURE']:"";
         $my_sign=sign::sha256($webhooks,$git_config["sign_key"]);
         if($sign!=$my_sign&&$action!="check"){//验证签名
             Log::info("sign error!");
             return "sign error!";
         }
         Log::info($webhooks);
         $webhooks=json_decode($webhooks,true);
         //操作判断
         if($action==""){
             return "not any action!";
         }
         else if($action=="check"){
             return $this->check();
         }
         else if($action=="version"){
             $full_name=isset($webhooks['full_name'])?$webhooks['full_name']:"";
             return $this->version($full_name);
         }
         else if($action=="update"){
             $full_name=isset($webhooks['full_name'])?$webhooks['full_name']:"";
             return $this->update($full_name);
         }
         else if($action=="deploy"){
             return $this->deploy($webhooks);
         }
    }
    
    /**
     * 检查版本号
     * */
    private function check(){
        $git_config=db::get_git_config();
        $data=array("full_name"=>$git_config["full_name"]);
        $data=json_encode($data);
        $sign=sign::sha256($data,$git_config["sign_key"]);
        $webhooks=Http::Post($git_config['version'],$data,array("Content-Type:application/json;charset=utf-8;","X-Gogs-Signature:$sign"));
        if($webhooks!=="updated"&&$webhooks!=='nothing'&&$webhooks!=='sign error!'){
            $this->deploy(json_decode($webhooks,true));
            Http::Post($git_config['update'],$data,array("Content-Type:application/json;charset=utf-8;","X-Gogs-Signature:$sign"));
            return $webhooks;
        }
        else{
            return $webhooks;
        }
    }
    
    /**
     * 获取版本
     * @param string $full_name GIT项目名称
     * @return string 处理结果
     * */
    private function version($full_name){
        $result=db::index_webhooks($full_name);
        if($result===false){
            return "nothing";
        }
        else if($result['status']==="0"){
            return json_encode($result);
        }
        else{
            return "updated";
        }
    }
    
    /**
     * 更新版本号
     * @param string $full_name GIT项目名称
     * @return string 处理结果
     * */
    private function update($full_name){
        $result=db::index_webhooks($full_name);
        if($result===false){
            return "nothing";
        }
        else if($result['status']==="0"){
            $result['status']="1";
            db::update_webhooks($full_name,$result);
            return "success";
        }
        else{
            return "updated";
        }
    }
    
    /**
     * 执行部署
     * @param array $webhooks GIT钩子消息
     * @return string 处理结果
     * */
     private function deploy($webhooks){
         $full_name=isset($webhooks['repository']['full_name'])?$webhooks['repository']['full_name']:"";
         $ref=isset($webhooks['ref'])?$webhooks['ref']:"";
         $git_config=db::get_git_config();
         if($git_config["full_name"]===$full_name){//本地节点部署项目更新
            $result=Git::run($git_config['ref'][$ref]);
            Log::info($result);
            return $result;
         }
         else if($full_name!=""){//非本地部署项目存储版本数据,等待其他节点读取
            Log::info("此节点没有部署该项目!");
            $webhooks['status']="0";
            db::update_webhooks($full_name,$webhooks);
            return "success";
         }
         else{
            Log::info("未知错误!");
            return "error";
         }
     }
}