<?php
/**
 * 日志操作
 * */
class Log{
    
    /**
     * 写入
     * */
    private static function write($msg){
        $fp = fopen(ROOT_PATH.'/logs/run_'.date('Ymd').'.log','a+');
        fwrite($fp,$msg);
        fclose($fp);
    }
    
    /**
     * 运行日志模板
     * */
    public static function info($msg){
        $header="[ ".date('Y-m-d H:i:s')." ] \n[ info ] ";
        $footer="\n---------------------------------------------------------------\n";
        self::write($header.$msg.$footer);
    }
}