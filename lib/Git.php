<?php
/**
 * 远程拉取更新
 * */
class Git{
    /**
     * @param string $ref 项目分支名称
     * @return string 运行结果
     * */
    public static function run($command){
        Log::info($command);
        return system($command);
    }
}