<?php
/**
 * 文件数据存取操作
 * */

class db{
    //取
    private static function get_php_file($filename) {
        return trim(substr(file_get_contents(ROOT_PATH.$filename), 15));
    }
    //存
    private static function set_php_file($filename, $content) {
        $fp = fopen(ROOT_PATH.$filename, "w");
        fwrite($fp, "<?php exit();?>" . $content);
        fclose($fp);
    }
    //查询git.config
    public static function get_git_config(){
        return json_decode(self::get_php_file("/config/git.config.php"),true);
    }
    //查询Webhooks
    public static function get_webhooks(){
        return json_decode(self::get_php_file("/data/Webhooks.php"),true);
    }
    //写入Webhooks
    public static function set_webhooks($data){
        return self::set_php_file("/data/Webhooks.php",json_encode($data));
    }
    //索引Webhooks,key值为webhooks消息中的full_name节点名称
    public static function index_webhooks($key){
        $webhooks=self::get_webhooks();
        if(isset($webhooks[$key])){
            return $webhooks[$key];
        }
        else{
            return false;
        }
    }
    //更新Webhooks,key值为webhooks消息中的full_name节点名称
    public static function update_webhooks($key,$data){
        $webhooks=self::get_webhooks();
        $webhooks[$key]=$data;
        self::set_webhooks($webhooks);
        return true;
    }
}