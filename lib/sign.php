<?php
/**
 * 生成签名
 * */
class sign{
    /**
     * @param string $str 签名原字符串
     * @parma string $key 签名密钥
     * @return string 签名后字符串
     * */
    public static function sha256($str,$key){
        return hash_hmac('sha256', $str, $key);
    }
}